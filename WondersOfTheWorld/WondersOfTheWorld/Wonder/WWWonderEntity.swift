//
//  WWWonderEntity.swift
//  WondersOfTheWorld
//
//  Created by Gerardo Naranjo on 29/03/22.
//

import Foundation

// MARK: Struct
struct WWWonderEntity {
    let title: String       // name for the wondder
    let description: String
}
