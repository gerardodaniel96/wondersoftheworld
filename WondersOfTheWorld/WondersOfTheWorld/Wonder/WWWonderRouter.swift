//
//  WWWonderRouter.swift
//  WondersOfTheWorld
//
//  Created by Gerardo Naranjo on 29/03/22.
//

import UIKit

typealias EntryPoint = AnyView & UIViewController

// MARK: Protocol
protocol AnyRouter {
    var entry: EntryPoint? { get set }
    static func start() -> AnyRouter
}

// MARK: Class
class WWWonderRouter: AnyRouter {

    /// Variable for entry point of the view.
    var entry: EntryPoint?

    /// Function that will be called to initialize the Router.
    /// - Returns: Router.
    static func start() -> AnyRouter {
        let router = WWWonderRouter()

        var view: AnyView = WWWonderView()
        var presenter: AnyPresenter = WWWonderPresenter()
        var interactor: AnyInteractor = WWWonderInteractor()

        view.presenter = presenter

        interactor.presenter = presenter

        presenter.router = router
        presenter.view = view
        presenter.interactor = interactor

        router.entry = view as? EntryPoint

        return router
    }

}
