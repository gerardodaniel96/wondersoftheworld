//
//  WWWonderInteractor.swift
//  WondersOfTheWorld
//
//  Created by Gerardo Naranjo on 29/03/22.
//

import Foundation

// MARK: Protocol
protocol AnyInteractor {
    var presenter: AnyPresenter? { get set }
    func setSevenWonders()
}

// MARK: Class
class WWWonderInteractor: AnyInteractor {

    /// Presenter.
    var presenter: AnyPresenter?

    /// Set the wonders.
    func setSevenWonders() {
        let wonderEntitys: [WWWonderEntity] = [
            WWWonderEntity(
                title: NSLocalizedString("ib.WWWonderInteractor.tableViewCellText.firstWonder", comment: "First Wonder Title"),
                description: NSLocalizedString("ib.WWWonderInteractor.tableViewCellSecondaryText.firstWonder", comment: "First Wonder Description")
            ),
            WWWonderEntity(
                title: NSLocalizedString("ib.WWWonderInteractor.tableViewCellText.secondWonder", comment: "Second Wonder Title"),
                description: NSLocalizedString("ib.WWWonderInteractor.tableViewCellSecondaryText.secondWonder", comment: "Second Wonder Description")
            ),
            WWWonderEntity(
                title: NSLocalizedString("ib.WWWonderInteractor.tableViewCellText.thirdWonder", comment: "Third Wonder Title"),
                description: NSLocalizedString("ib.WWWonderInteractor.tableViewCellSecondaryText.thirdWonder", comment: "Third Wonder Description")
            ),
            WWWonderEntity(
                title: NSLocalizedString("ib.WWWonderInteractor.tableViewCellText.fourthWonder", comment: "Fourth Wonder Title"),
                description: NSLocalizedString("ib.WWWonderInteractor.tableViewCellSecondaryText.fourthWonder", comment: "Fourth Wonder Description")
            ),
            WWWonderEntity(
                title: NSLocalizedString("ib.WWWonderInteractor.tableViewCellText.fifthWonder", comment: "Fifth Wonder Title"),
                description: NSLocalizedString("ib.WWWonderInteractor.tableViewCellSecondaryText.fifthWonder", comment: "Fifth Wonder Description")
            ),
            WWWonderEntity(
                title: NSLocalizedString("ib.WWWonderInteractor.tableViewCellText.sixthWonder", comment: "Sixth Wonder Title"),
                description: NSLocalizedString("ib.WWWonderInteractor.tableViewCellSecondaryText.sixthWonder", comment: "Sixth Wonder Description")
            ),
            WWWonderEntity(
                title: NSLocalizedString("ib.WWWonderInteractor.tableViewCellText.seventhWonder", comment: "Seventh Wonder Title"),
                description: NSLocalizedString("ib.WWWonderInteractor.tableViewCellSecondaryText.seventhWonder", comment: "Seventh Wonder Description")
            )
        ]
        presenter?.interactorSetWonders(with: .success(wonderEntitys))
    }
}
