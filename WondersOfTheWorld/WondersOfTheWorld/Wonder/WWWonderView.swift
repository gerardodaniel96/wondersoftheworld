//
//  WWWonderView.swift
//  WondersOfTheWorld
//
//  Created by Gerardo Naranjo on 29/03/22.
//

import UIKit

// MARK: Protocol
protocol AnyView {
    var presenter: AnyPresenter? { get set }
    func setWonders(errorMessage: String)
    func setWonders(wonders: [WWWonderEntity])
}

// MARK: Class
class WWWonderView: UIViewController, AnyView, UITableViewDelegate, UITableViewDataSource {

    /// Presenter
    var presenter: AnyPresenter?

    /// List of wonders of the current view.
    private var wonders: [WWWonderEntity] = []

    /// This method is called when there is a failure configuring the wonders and will display an alert to the user.
    /// - Parameter errorMessage: text to be displayed on the Alert.
    func setWonders(errorMessage: String) {
        // TODO: Display Alert with message
    }

    /// This method is called when wonders are configured correctly, it will update the variable and the view to display them.
    /// - Parameter wonders: wonders to show.
    func setWonders(wonders: [WWWonderEntity]) {
        DispatchQueue.main.async {
            self.wonders = wonders
            self.tableView.reloadData()
        }
    }

    /// Variable for TableView.
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "wonderCell")
        return table
    }()

    /// Called after the controller's view is loaded into memory.
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
    }

    /// Called to notify the view controller that its view has just laid out its subviews.
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
        tableView.separatorColor = .black
    }

    /// Tells the data source to return the number of rows in a given section of a table view.
    /// - Parameters:
    ///   - tableView: The table-view object requesting this information.
    ///   - section: An index number identifying a section in tableView.
    /// - Returns: The number of rows in section.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wonders.count
    }

    /// Asks the data source for a cell to insert in a particular location of the table view.
    /// - Parameters:
    ///   - tableView: A table-view object requesting the cell.
    ///   - indexPath: An index path locating a row in tableView.
    /// - Returns: An object inheriting from UITableViewCell that the table view can use for the specified row. UIKit raises an assertion if you return nil.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wonderCell", for: indexPath)

        var content = cell.defaultContentConfiguration()
        content.text = wonders[indexPath.row].title
        content.secondaryText = wonders[indexPath.row].description
        content.textProperties.numberOfLines = 0
        content.textProperties.color = .systemOrange
        content.textProperties.font = .boldSystemFont(ofSize: 22)
        content.secondaryTextProperties.numberOfLines = 0
        content.secondaryTextProperties.font = .systemFont(ofSize: 12, weight: .light)
        cell.contentConfiguration = content

        cell.separatorInset = UIEdgeInsets.init(top: 0.0, left: 32.0, bottom: 0.0, right: 32.0)

        return cell
    }

}
