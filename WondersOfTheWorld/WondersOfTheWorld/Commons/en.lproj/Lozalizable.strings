/* 
  Lozalizable.strings
  WondersOfTheWorld

  Created by Gerardo Naranjo on 29/03/22.
  
*/

"ib.WWWonderInteractor.tableViewCellText.firstWonder" = "Chichén Itzá pyrammid at Chichén Itzá, Yucatan peninsula, Mexico";
"ib.WWWonderInteractor.tableViewCellSecondaryText.firstWonder" = "El Castillo, also known as the Temple of Kukulcan, is a Mesoamerican step-pyramid that dominates the center of the Chichen Itza archaeological site in the Mexican state of Yucatán. The building is more formally designated by archaeologists as Chichen Itza Structure 5B18.
    
    Built by the pre-Columbian Maya civilization sometime between the 9th and 12th centuries CE, El Castillo served as a temple to the god Kukulkan, the Yucatec Maya Feathered Serpent deity closely related to the god Quetzalcoatl known to the Aztecs and other central Mexican cultures of the Postclassic period.

    The pyramid consists of a series of square terraces with stairways up each of the four sides to the temple on top. Sculptures of plumed serpents run down the sides of the northern balustrade. During the spring and autumn equinoxes, the late afternoon sun strikes off the northwest corner of the pyramid and casts a series of triangular shadows against the northwest balustrade, creating the illusion of a feathered serpent “crawling” down the pyramid. The event has been very popular, but it is questionable whether it is a result of a purposeful design. Each of the pyramid’s four sides has 91 steps which, when added together and including the temple platform on top as the final “step”, produces a total of 365 steps (which is equal to the number of days of the Haab’ year).

    The structure is 24 m (79 ft) high, plus an additional 6 m (20 ft) for the temple. The square base measures 55.3 m (181 ft) across.

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.secondWonder" = "Christ the Redeemer, Rio de Janeiro, Brazil";
"ib.WWWonderInteractor.tableViewCellSecondaryText.secondWonder" = "Christ the Redeemer is an Art Deco statue of Jesus Christ in Rio de Janeiro, Brazil, created by Polish-French sculptor Paul Landowski and built by the Brazilian engineer Heitor da Silva Costa, in collaboration with the French engineer Albert Caquot. The face was created by the Romanian artist Gheorghe Leonida. The statue is 30 metres (98 ft) tall, not including its 8-metre (26 ft) pedestal, and its arms stretch 28 metres (92 ft) wide. By comparison, it is approximately two-thirds the height of the Statue of Liberty’s height from base to torch.

    The statue weighs 635 metric tons (625 long, 700 short tons), and is located at the peak of the 700-metre (2,300 ft) Corcovado mountain in the Tijuca Forest National Park overlooking the city of Rio. A symbol of Christianity across the world, the statue has also become a cultural icon of both Rio de Janeiro and Brazil, and is listed as one of the New Seven Wonders of the World. It is made of reinforced concrete and soapstone, and was constructed between 1922 and 1931.

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.thirdWonder" = "Machu Picchu, Cusco Region, Peru";
"ib.WWWonderInteractor.tableViewCellSecondaryText.thirdWonder" = "Machu Picchu, is a 15th-century Inca citadel situated on a mountain ridge 2,430 metres (7,970 ft) above sea level. It is located in the Cusco Region, Urubamba Province, Machupicchu District in Peru, above the Sacred Valley, which is 80 kilometres (50 mi) northwest of Cuzco and through which the Urubamba River flows.

    Most archaeologists believe that Machu Picchu was built as an estate for the Inca emperor Pachacuti (1438–1472). Often mistakenly referred to as the “Lost City of the Incas” (a title more accurately applied to Vilcabamba), it is the most familiar icon of Inca civilization. The Incas built the estate around 1450 but abandoned it a century later at the time of the Spanish Conquest. Although known locally, it was not known to the Spanish during the colonial period and remained unknown to the outside world until American historian Hiram Bingham brought it to international attention in 1911.

    Machu Picchu was built in the classical Inca style, with polished dry-stone walls. Its three primary structures are the Inti Watana, the Temple of the Sun, and the Room of the Three Windows. Most of the outlying buildings have been reconstructed in order to give tourists a better idea of how they originally appeared. By 1976, thirty percent of Machu Picchu had been restored and restoration continues.

    Machu Picchu was declared a Peruvian Historical Sanctuary in 1981 and a UNESCO World Heritage Site in 1983. In 2007, Machu Picchu was voted one of the New Seven Wonders of the World in a worldwide Internet poll.

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.fourthWonder" = "Taj Mahal, Agra & Uttar Pradesh, India";
"ib.WWWonderInteractor.tableViewCellSecondaryText.fourthWonder" = "The Taj Mahal is an ivory-white marble mausoleum on the south bank of the Yamuna river in the Indian city of Agra. In 2007, it was declared a winner of the New7Wonders of the World (2000–2007) initiative.

    It was commissioned in 1632 by the Mughal emperor, Shah Jahan (reigned 1628–1658), to house the tomb of his favorite wife, Mumtaz Mahal. The tomb is the centrepiece of a 42-acre complex, which includes a mosque and a guest house, and is set in formal gardens bounded on three sides by a crenellated wall.

    Construction of the mausoleum was essentially completed in 1643 but work continued on other phases of the project for another 10 years. The Taj Mahal complex is believed to have been completed in its entirety in 1653 at a cost estimated at the time to be around 32 million rupees, which in 2015 would be approximately 52.8 billion rupees (US$827 million). The construction project employed some 20,000 artisans under the guidance of a board of architects led by the court architect to the emperor, Ustad Ahmad Lahauri.

    The Taj Mahal was designated as a UNESCO World Heritage Site in 1983 for being “the jewel of Muslim art in India and one of the universally admired masterpieces of the world’s heritage”. Described by Nobel laureate Rabindranath Tagore as “the tear-drop on the cheek of time”, it is regarded by many as the best example of Mughal architecture and a symbol of India’s rich history. The Taj Mahal attracts 7–8 million visitors a year.
    
    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.fifthWonder" = "Petra, Ma'an Governorate, Jordan";
"ib.WWWonderInteractor.tableViewCellSecondaryText.fifthWonder" = "Petra, originally known to the Nabataeans as Raqmu, is a historical and archaeological city in southern Jordan. The city is famous for its rock-cut architecture and water conduit system. Another name for Petra is the Rose City due to the color of the stone out of which it is carved.

    Established possibly as early as 312 BC as the capital city of the Arab Nabataeans, it is a symbol of Jordan, as well as Jordan’s most-visited tourist attraction. The Nabateans were nomadic Arabs who benefited from the proximity of Petra to the regional trade routes, in becoming a major trading hub, thus enabling them to gather wealth. The Nabateans are also known for their great ability in constructing efficient water collecting methods in the barren deserts and their talent in carving structures into solid rocks. It lies on the slope of Jebel al-Madhbah (identified by some as the biblical Mount Hor) in a basin among the mountains which form the eastern flank of Arabah (Wadi Araba), the large valley running from the Dead Sea to the Gulf of Aqaba. Petra has been a UNESCO World Heritage Site since 1985.

    The site remained unknown to the western world until 1812, when it was introduced by Swiss explorer Johann Ludwig Burckhardt. It was described as “a rose-red city half as old as time” in a Newdigate Prize-winning poem by John William Burgon. UNESCO has described it as “one of the most precious cultural properties of man’s cultural heritage”. Petra was named amongst the New7Wonders of the World in 2007 and was also chosen by the Smithsonian Magazine as one of the “28 Places to See Before You Die”.

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.sixthWonder" = "Colosseum, Rome, Italy";
"ib.WWWonderInteractor.tableViewCellSecondaryText.sixthWonder" = "The Colosseum or Coliseum, also known as the Flavian Amphitheatre, is an oval amphitheatre in the centre of the city of Rome, Italy. Built of concrete and sand, it is the largest amphitheatre ever built. The Colosseum is situated just east of the Roman Forum. Construction began under the emperor Vespasian in AD 72, and was completed in AD 80 under his successor and heir Titus. Further modifications were made during the reign of Domitian (81–96). These three emperors are known as the Flavian dynasty, and the amphitheatre was named in Latin for its association with their family name (Flavius).

    The Colosseum could hold, it is estimated, between 50,000 and 80,000 spectators, having an average audience of some 65,000; it was used for gladiatorial contests and public spectacles such as mock sea battles (for only a short time as the hypogeum was soon filled in with mechanisms to support the other activities), animal hunts, executions, re-enactments of famous battles, and dramas based on Classical mythology. The building ceased to be used for entertainment in the early medieval era. It was later reused for such purposes as housing, workshops, quarters for a religious order, a fortress, a quarry, and a Christian shrine.

    Although partially ruined because of damage caused by earthquakes and stone-robbers, the Colosseum is still an iconic symbol of Imperial Rome. It is one of Rome’s most popular tourist attractions and has also links to the Roman Catholic Church, as each Good Friday the Pope leads a torchlit “Way of the Cross” procession that starts in the area around the Colosseum.

    The Colosseum is also depicted on the Italian version of the five-cent euro coin.

    The Colosseum’s original Latin name was Amphitheatrum Flavium, often anglicized as Flavian Amphitheater. The building was constructed by emperors of the Flavian dynasty, following the reign of Nero. This name is still used in modern English, but generally the structure is better known as the Colosseum. In antiquity, Romans may have referred to the Colosseum by the unofficial name Amphitheatrum Caesareum (with Caesareum an adjective pertaining to the title Caesar), but this name may have been strictly poetic as it was not exclusive to the Colosseum; Vespasian and Titus, builders of the Colosseum, also constructed an amphitheater of the same name in Puteoli (modern Pozzuoli).

    The name Colosseum has long been believed to be derived from a colossal statue of Nero nearby (the statue of Nero was named after the Colossus of Rhodes). This statue was later remodeled by Nero’s successors into the likeness of Helios (Sol) or Apollo, the sun god, by adding the appropriate solar crown. Nero’s head was also replaced several times with the heads of succeeding emperors. Despite its pagan links, the statue remained standing well into the medieval era and was credited with magical powers. It came to be seen as an iconic symbol of the permanence of Rome.

    In the 8th century, a famous epigram attributed to the Venerable Bede celebrated the symbolic significance of the statue in a prophecy that is variously quoted: Quamdiu stat Colisæus, stat et Roma; quando cadet colisæus, cadet et Roma; quando cadet Roma, cadet et mundus (“as long as the Colossus stands, so shall Rome; when the Colossus falls, Rome shall fall; when Rome falls, so falls the world”). This is often mistranslated to refer to the Colosseum rather than the Colossus (as in, for instance, Byron’s poem Childe Harold’s Pilgrimage). However, at the time that the Pseudo-Bede wrote, the masculine noun coliseus was applied to the statue rather than to what was still known as the Flavian amphitheatre.

    The Colossus did eventually fall, possibly being pulled down to reuse its bronze. By the year 1000 the name “Colosseum” had been coined to refer to the amphitheatre. The statue itself was largely forgotten and only its base survives, situated between the Colosseum and the nearby Temple of Venus and Roma.

    The name further evolved to Coliseum during the Middle Ages. In Italy, the amphitheatre is still known as il Colosseo, and other Romance languages have come to use similar forms such as Coloseumul (Romanian), le Colisée (French), el Coliseo (Spanish) and o Coliseu (Portuguese).
    
    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.seventhWonder" = "Great Wall of China";
"ib.WWWonderInteractor.tableViewCellSecondaryText.seventhWonder" = "The Great Wall of China is a series of fortifications made of stone, brick, tamped earth, wood, and other materials, generally built along an east-to-west line across the historical northern borders of China to protect the Chinese states and empires against the raids and invasions of the various nomadic groups of the Eurasian Steppe. Several walls were being built as early as the 7th century BC; these, later joined together and made bigger and stronger, are now collectively referred to as the Great Wall. Especially famous is the wall built 220–206 BC by Qin Shi Huang, the first Emperor of China. Little of that wall remains. Since then, the Great Wall has on and off been rebuilt, maintained, and enhanced; the majority of the existing wall is from the Ming Dynasty.

    Other purposes of the Great Wall have included border controls, allowing the imposition of duties on goods transported along the Silk Road, regulation or encouragement of trade and the control of immigration and emigration. Furthermore, the defensive characteristics of the Great Wall were enhanced by the construction of watch towers, troop barracks, garrison stations, signaling capabilities through the means of smoke or fire, and the fact that the path of the Great Wall also served as a transportation corridor.

    The Great Wall stretches from Dandong in the east, to Lop Lake in the west, along an arc that roughly delineates the southern edge of Inner Mongolia. A comprehensive archaeological survey, using advanced technologies, has concluded that the Ming walls measure 8,850 km (5,500 mi). This is made up of 6,259 km (3,889 mi) sections of actual wall, 359 km (223 mi) of trenches and 2,232 km (1,387 mi) of natural defensive barriers such as hills and rivers. Another archaeological survey found that the entire wall with all of its branches measure out to be 21,196 km (13,171 mi).
    
    - https://world.new7wonders.com";
